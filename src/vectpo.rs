
pub trait Element: Copy + PartialEq {
    fn id() -> u32;
    fn label() -> string;
}


struct Poset<T : Element> {
    width : usize,
    height : usize,
    contents : Vec<T>
}




